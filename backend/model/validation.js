
const mongoose = require("mongoose")
const Schema = mongoose.Schema;
const joi = require("joi")


module.exports.joiSchemaUser = joi.object({
    firstname: joi.string().required().min(3).max(10),
    lastname: joi.string().required().min(3),
    email: joi.string().required().email(),
    password : joi.string().required().min(6),
    dialcode: joi.string().required(),
    phoneNo: joi.string().required().min(6).max(10),
})

module.exports.joiSchemapost= joi.object({
    userId: joi.string().required(),
    postlink: joi.string().required()
})