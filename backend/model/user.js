const mongoose = require("mongoose")
const Schema = mongoose.Schema;
const joi = require("joi")
const bcrypt = require("bcrypt")


const userSchema = new Schema({
    firstname:{
        type:String,
        required : true,
        minLength: 3,
    },
    lastname:{
        type:String,
        required : true,
        minLength: 3,
    },
    email:{
        type:String,
        required : true,
        unique: true,
    
    },
    password:{
        type:String,
        required : true,
        minLength: 6,
    },
    dialcode:{
        type:String,
        required : true,
        
    },
    phoneNo:{
        type:String,
        required : true,
    },
    validtoken:{
        type:String,
        
    }
   
})



userSchema.pre('save',async function(next){
    try{
        const salt= await bcrypt.genSalt(10);
        const hashh = await bcrypt.hash(this.password,salt);
        this.password=hashh;
        next();
    }
    catch(err)
    {
        next(err);
    }

})

const studentsSchema= mongoose.model("dummy_user",userSchema);
module.exports= {studentsSchema}








