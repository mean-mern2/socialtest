const User = require('../model/user');
const User2 = require('../model/posts');
const joi_validation = require('../model/validation');
const mongoose = require("mongoose");
const bcrypt = require("bcrypt")
const jwt = require("jsonwebtoken")
const { date, expression } = require('joi');

const secret_key = "abc@apptunix2121"

module.exports.register = async (req, res, next) => {
    try {
        
        
        let { firstname, lastname, email, password, dialcode, phoneNo } = req.body;
        await joi_validation.joiSchema.validateAsync(req.body)
        
        
        //---- validating dialcode----//
        if (!(Number(dialcode))) { throw ("Sorry enter a valid DialCode") }
        dialcode="+"+dialcode;
        
        
        //---- validating dial + number ----//
        const myuser2 = await User.studentsSchema.findOne({  email });
        if (myuser2) { throw ("Sorry this email already exist") }
        const myuser = await User.studentsSchema.findOne({  phoneNo, dialcode  });
        if (myuser ) { throw ("Sorry this number already exist") }
    
        
        
        
        let user = await User.studentsSchema.create({ firstname,  lastname, email, password, dialcode,  phoneNo });
        if (!user) 
        {        res.status(400).json({ message: "unable to register student" })    }
        
        return res.status(201).json({ user });

    }
    catch (err) {
        console.log(err, "-----")
        return res.status(400).json({ message: err });
    }
}

module.exports.loginpage = async (req, res, next) => {
    try {
        const email = req.body.email;
        let dialcode = req.body.dialcode;
        const phoneNo = req.body.phoneNo;
        const password = req.body.password;
        let myuser;
        

        if (email && (dialcode || phoneNo)) { res.send("provide either email or phone no.") }

        if (email) 
        {    myuser = await User.studentsSchema.findOne({ "email": email });    
            if (!myuser)
                {    return res.status(404).json({ message: "user not found" })   }
        }
        else if (dialcode && phoneNo)
        {
            if(dialcode[0]!='+')
            {
                dialcode="+"+dialcode;
            }
            myuser = await User.studentsSchema.findOne({ "phoneNo": phoneNo,"dialcode":dialcode });
            if (!myuser)
            {    return res.status(404).json({ message: "user not found" })   }
            
        
        }
        else
        {    return res.status(404).json({ message: "Wrong Credentials" })  }
        
        let pas;
        if (password) 
        {  
            pas = await bcrypt.compare(password, myuser.password) 
            if(!pas)
            {res.json("password incorrect") }        
        
        }
        else 
        {  throw ("password not provided")  }

        

        
        const idd= myuser._id

         const   jti=Date.now().toString()
        
        const authtoken = jwt.sign({idd,jti}, secret_key, { expiresIn: "2h" });
        const updateuser = await User.studentsSchema.findByIdAndUpdate(idd,{$set:{"validtoken": jti}})
        

        return res.json({ authtoken })

      
    }
    catch (err) {
        console.log(err);
        next(err);
    }
}

module.exports.getprofile = async (req, res, next) => {
    try {
        const token1 = req.header("auth-token");
        let myuser;
       
            const decoded =  await jwt.verify(token1, secret_key, (err, decoded1) => {
                return decoded1;
            });
            
        
            myuser = await User.studentsSchema.findById(decoded.idd);
            if(myuser)
            {
               
                if (decoded.jti != myuser.validtoken) 
                {
                    throw ("JWT ID is invalid");
                }
            }
            else
            {
                return res.status(400).json({message:"user not found"})
            }
      
            next()
        
        
    }
    catch (err) {
        next(err);
    }
}

module.exports.sendfile = async(req,res,next)=>{
    try {
        res.send(req.file);
      }catch(err) {
        res.send(400);
      }
}

module.exports.uploadpost= async (req,res,next)=>{
    try{

        const token1 = req.header("auth-token");
        let myuser;
       
            const decoded =  await jwt.verify(token1, secret_key, (err, decoded1) => {
                return decoded1;
            });
            
        
            myuser = await User.studentsSchema.findById(decoded.idd);
            if(myuser)
            {
               
                if (decoded.jti != myuser.validtoken) 
                {
                    throw ("JWT ID is invalid");
                }
            }
            else
            {
                return res.status(400).json({message:"user not found"})
            }

        


            
           const postlink = req.body.postlink;
            
            
            const myuser2 = await User2.postSchema.create({ "userId":decoded.idd,"postlink": postlink});
            if(!myuser2)
            
            {        res.status(400).json({ message: "unable to upload post" })    }            
            return res.status(201).json({ myuser2 });
            
            
    }
    catch(err)
    {
        return err;
    }
}

module.exports.deletepost= async (req,res,next)=>{
    try{

        const token1 = req.header("auth-token");
        const postId = req.body.postId;
        let myuser;
       
            const decoded =  await jwt.verify(token1, secret_key, (err, decoded1) => {
                return decoded1;
            });
            
            const user = await User.studentsSchema.findById(decoded.idd);
            
            if(user)
            {
                
                if (decoded.jti != user.validtoken) 
                {
                    throw ("JWT ID is invalid");
                }
            }
            else
            {
                return res.status(400).json({message:"user not found"})
            }
            if(!user)
            {
                return res.json({message:"user not found"})
            }


            myuser = await User2.postSchema.findOne({"_id":postId,"userId":decoded.idd});
            
            
            
            const myuser2 = await User2.postSchema.deleteOne({ "_id":postId,"userId":decoded.idd});
            
            
            if(!myuser2)
            
            {        res.status(400).json({ message: "unable to delete post" })    }            
            console.log("5");
            return res.status(201).json({ myuser2 });
            
            
    }
    catch(err)
    {
        return err;
    }
}
