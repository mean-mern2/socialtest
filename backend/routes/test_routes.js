const express = require("express");
const users = require("../controller/test_controller");
const router =express.Router();

const multer = require('multer');


var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './social_media');
     },
    filename: function (req, file, cb) {
        console.log(file,"111")
        cb(null ,  file.originalname);
    }
});

var upload = multer({ storage: storage })

router.post("/postdata",users.register);
router.get("/login",users.loginpage);
router.post("/sendfile",users.getprofile,upload.single('file'),users.sendfile);
router.post("/uploadpost",users.uploadpost)
router.post("/deletepost",users.deletepost)

module.exports=router;